import random
import os
random.seed(os.times())

count = 0
switch_won = 0
no_switch_won = 0
limit = 100000

while(count < limit):
    fild = [0, 0, 0]
    chose_door_change = 0
    chose_door_no_change = 0

    fild[random.randrange(0, 3)] = 1
    chose_door_change = random.randrange(0, 3)
    chose_door_no_change = random.randrange(0, 3)

    if fild[chose_door_no_change] == 1:
        no_switch_won += 1

    fild[chose_door_change] = 0
    if fild.count(1) != 0:
        switch_won += 1

    count += 1
    #print(count)

print("no switch:" + str((no_switch_won/limit) * 100) + "%" + " | " + "switch:" + str((switch_won/limit) * 100) + "%")